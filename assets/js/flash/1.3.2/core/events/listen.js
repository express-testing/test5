/**
 * Add an event listener to a node, a node list and eventually with delegation
 * @param  {Node|Nodelist}  	nodes    the node/nodes to bind the event
 * @param  {String}   			event    the name of the vent
 * @param  {Function} 			fn       the callback of the event
 * @param  {string}   			selector the selector used for delegation
 * @return {void}            	
 */
flashCore.prototype.listen = function(nodes, event, fn, selector) {
	//var self = this;
	var callback = fn;
	
	// Return if nodes is not a nodelist neither a node
	if(!nodes || (!NodeList.prototype.isPrototypeOf(nodes) && !Node.prototype.isPrototypeOf(nodes) && !HTMLCollection.prototype.isPrototypeOf(nodes) && typeof nodes !== 'string')) {
		return;
	} else if(Node.prototype.isPrototypeOf(nodes)) {
		nodes = [nodes];
	} else if(typeof nodes === 'string') {
		nodes = document.querySelectorAll(nodes);
	} else if(HTMLCollection.prototype.isPrototypeOf(nodes)) {
		nodes = Array.prototype.slice.call(nodes);
	}

	// Checking if delegation is needed
	var delegate = !!selector;
	if(delegate) {
		callback = function(event) {
			var target = event.target;
			var found = false;
			while(target.parentNode) {
				if(typeof target.matches !== 'undefined' && target.matches(selector)) {
					fn(event, target);
				} 
				target = target.parentNode;
			}
		}
	}

	// Add Listeners
	var events = event.split(' ');
    for (var i = 0, len = nodes.length; i < len; i++) {
    	events.forEach(function(single_event){
 			nodes[i].addEventListener(single_event, callback);
    	});
    }
}