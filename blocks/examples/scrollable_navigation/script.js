// ======================================================
// Block Javascript
// ================
/*
$(document).ready(function() {
	var $block = $('.examples_scrollable_navigation');
	if($block.length && !$('body').hasClass('js_init--examples_scrollable_navigation')) {
		$('body').addClass('js_init--examples_scrollable_navigation');
		
		var $container = $block.find('div.horizontal_menu__content');
		var $links = $block.find('div.horizontal_menu__scroll');
		var $next = $block.find('div.horizontal_menu__next');
		var $prev = $block.find('div.horizontal_menu__prev');
		var $overview = $block.find('div.horizontal_menu__container').find('a.horizontal_menu__link').first();
		var interval;
		var speed = 2;

		// Next & prev scroll on hover
		$next.mouseover(function() {
			interval = setInterval(function() {
				$container.scrollLeft($container.scrollLeft() + speed);
		   	}, speed);
		});
		$prev.mouseover(function() {
			interval = setInterval(function() {
				$container.scrollLeft($container.scrollLeft() - speed);
		   	}, speed);
		});
		$prev.mouseout(function() { clearInterval(interval); });
		$next.mouseout(function() { clearInterval(interval); });

		// Show hide next & prev buttons based on scroll positions
		$container.scroll(function() {
			if($container.scrollLeft() > 0) $prev.show();
			else $prev.hide();
			var scroll = $container.scrollLeft();
			var leftWidth = $links.get(0).scrollWidth - scroll;
			if(leftWidth - 1 <= $container.width()) $next.hide();
			else $next.show();
		});

		// Show / hide next button depending on links container width
		function update() {
			if($links.outerWidth() > $container.outerWidth()) $next.show();
			else $next.hide();
		}
		$(window).resize(update);
		update();

		// It's a bit ugly atm but it's the only way to make it
		// work "nicely" on the front end. And it doesn't consume
		// that much CPU at all.
		setInterval(function() {
			$prev.css('left', parseInt($overview.outerWidth()));
		}, 100);

	}
});
*/